M.AutoInit();

const upperCaseAlp = ["#", "A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"];
let defaultRowCount = 20;
let defaultColCount = upperCaseAlp.length;

createHeaderRow = () => {
    const tr = document.createElement("tr");
    tr.setAttribute("id", "ss-h-0");
    for (let i = 0; i < upperCaseAlp.length; i++) {
        const th = document.createElement("th");
        th.setAttribute("id", `ss-h-0-${upperCaseAlp[i]}`);
        th.setAttribute("class", `${i === 0 ? "" : "indigo lighten-5"}`);
        if (i !== 0) {
          const span = document.createElement("span");
          span.innerHTML = `${upperCaseAlp[i]}`;
          span.setAttribute("class", "column-header-span");
          th.appendChild(span);
        }
        tr.appendChild(th);
    }
    return tr;
};

createTableBody = tableBody => {
    for (let rowNum = 1; rowNum <= defaultRowCount; rowNum++) {
      tableBody.appendChild(this.createTableBodyRow(rowNum));
    }
};

createTableBodyRow = rowNum => {
    const tr = document.createElement("tr");
    tr.setAttribute("id", `ss-r-${rowNum}`);
    for (let i = 0; i <= defaultColCount; i++) {
      const cell = document.createElement(`${i === 0 ? "th" : "td"}`);
      if (i === 0) {
        cell.contentEditable = false;
        const span = document.createElement("span");
        span.innerHTML = rowNum;
        cell.appendChild(span); 
        cell.setAttribute("class", "indigo lighten-5");
      } else {
        cell.contentEditable = false;
      }
      cell.setAttribute("id", `ss-r-${rowNum}-${i}`);
      tr.appendChild(cell);
    }
    return tr;
  };



const tableHeaderElement = document.getElementById("ss-main-header");
const tableBodyElement = document.getElementById("ss-main-body");

const tableBody = tableBodyElement.cloneNode(true);
tableBodyElement.parentNode.replaceChild(tableBody, tableBodyElement);
const tableHeaders = tableHeaderElement.cloneNode(true);
tableHeaderElement.parentNode.replaceChild(tableHeaders, tableHeaderElement);

tableHeaders.innerHTML = "";
tableBody.innerHTML = "";

tableHeaders.appendChild(createHeaderRow());
createTableBody(tableBody, defaultRowCount, defaultColCount);
let firstCell = document.getElementById("ss-r-1-1");
firstCell.focus();
firstCell.classList.add("focus-cell");

document.onkeydown = onKeyBoardOperations;

function onKeyBoardOperations(e) {
  e = e || window.event;
  if(e.keyCode == '38'){
    // up
    var index = firstCell.cellIndex;
    var nextrow = firstCell.parentElement.previousElementSibling;
    if (nextrow != null) {
      var sibling = nextrow.cells[index];
      moveToSibling(sibling);
    } 
  } else if(e.keyCode == '40'){
    // down
    var index = firstCell.cellIndex;
    var nextrow = firstCell.parentElement.nextElementSibling;
    if (nextrow != null) {
      var sibling = nextrow.cells[index];
      moveToSibling(sibling);
    } 
  } else if(e.keyCode == '37'){
    // left
    var sibling = firstCell.previousElementSibling;
    moveToSibling(sibling);
  } else if(e.keyCode == '39'){
    // right
    var sibling = firstCell.nextElementSibling;
    moveToSibling(sibling);
  } else if(e.keyCode == '113'){
    firstCell.setAttribute("contenteditable", true);
    moveToSibling(firstCell);
  }
}

moveToSibling = sibling => {
  if(sibling != null){
    firstCell.focus();
    firstCell.classList.remove("focus-cell");
    sibling.focus();
    sibling.classList.add("focus-cell");
    firstCell = sibling;
  }
}

document.querySelectorAll("#ss-main-sheet td").forEach(cell => {
  cell.addEventListener("click", (e) => {
    moveToSibling(e.currentTarget);
  });
  cell.addEventListener("blur", (e) => {
    e.currentTarget.classList.remove("focus-cell");
    moveToSibling(e.currentTarget);
  });
  cell.addEventListener("dblclick", (e) => {
    e.currentTarget.setAttribute("contenteditable", true);
    moveToSibling(e.currentTarget);
  });
});

saveSheet = () => {
  M.toast({html: 'Smart Sheet Saved!', classes: 'white-text indigo'});
}